FROM registry.cn-shenzhen.aliyuncs.com/dankal/php-nginx:7.1

ENV VIRTUAL_HOST jenkins_demo2.birdnight.cn

COPY . /app
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

RUN chmod -R 777 /app
